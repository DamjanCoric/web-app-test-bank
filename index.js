const express = require('express')
const axios = require('axios');
const qs = require('qs')
const bodyParser = require('body-parser')
const app = express()
const port = 9000;
const config = require('./config');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/',express.static('public'))


app.post('/test', async (req, res) => {
    let test = {}
    if (req.body.moment) {
        test = {
            moment: req.body.moment,
            action: req.body.action,
            code: req.body.code,
            message: req.body.message 
        }
    };
    const handle = (promise) => {
        return promise
            .then(data => ([data, undefined]))
            .catch(error => Promise.resolve([undefined, error]));
        }

        try {

    let [token, tokenError] = await handle(axios({
        method: 'post',
        url: `${config.url}/oauth/token`,
        headers: { 
            ...config.common_headers,
            'Content-Length': '105',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data:  qs.stringify({
            client_id: config.yellow_client_id,
            client_secret: config.yellow_client_secret
          })
    }))
    if(tokenError) throw new Error('Error while requesting token:'+tokenError.response.data.error);

    let [responseCreate, responseCreateError] = await handle(axios({
        method: 'post',
        url: `${config.url}/v1/transfer`,
        headers: { 
            ...config.common_headers,
            'Content-Length': '750',Authorization: `Bearer ${token.data.access_token}`,
            'x-api-key': config.yellow_x_api_key,
            'Content-Type': 'application/json' 
        },
        data: {
            "source": config.yellow_source,
            "target": config.yellow_target,
            "symbol": "$tin",
            "amount": "10",
            "labels": {
                "test": test,
                "description": "TST - Test bank web sapp",
                "domain": "tin",
                "type": "SEND",
                "sourceChannel": "APP"
            }
           }
    }))
    if(responseCreateError) throw new Error('Error while requesting transfer:'+JSON.stringify(responseCreateError.response.data.error));
    function timeout(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    await timeout(30000);

    let [responseAccept, responseAcceptError] = await handle(axios({
        method: 'post',
        url: `${config.url}/v1/transfer/${responseCreate.data.action_id}/accept`,
        headers: { 
            ...config.common_headers,
            'Content-Length': '750',
            Authorization: `Bearer ${token.data.access_token}`,
            'x-api-key': config.yellow_x_api_key,
            'Content-Type': 'application/json' 
        },
        data: {
            "signer":{ 
              "handle":config.yellow_source
            }, 
            "wallet": {
                "handle": config.yellow_target,
                 "signer": [config.yellow_source ]
            }
        },
    }))
    if (responseAcceptError) throw new Error('Error while accepting transfer:'+JSON.stringify(responseAcceptError.response.data.error));
    
    let [responseGetListOfActions, responseGetListOfActionsError] = await handle(axios({
        method: 'get',
        url: `${config.url}/v1/action?labels.tx_ref=${responseCreate.data.labels.tx_ref}`,
        headers:  { 
            ...config.common_headers,
            Authorization: `Bearer ${token.data.access_token}`,
            'x-api-key': config.yellow_x_api_key,
            },
    }))
    if (responseGetListOfActionsError) {
        throw new Error('Error while getting actions:'+JSON.stringify(responseGetListOfActionsError.response.data.error));
    }

    res.status(200).send(responseGetListOfActions.data);

} catch (error) { 
    console.log(error)
    res.status(200).send({err: error.message})
}
  })
app.listen(port, () => console.log(`Listening on port ${port}!`))